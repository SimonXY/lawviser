package buyakasha.pl.lawviser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import buyakasha.pl.lawviser.advisor.AdvisorLoginActivity;
import buyakasha.pl.lawviser.client.ClientLoginActivity;

public class MainActivity extends Activity {







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






        Button ClientButton = (Button) findViewById(R.id.Client);
        ClientButton.setOnClickListener(new View.OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                Intent clientRegisterRun = new Intent(MainActivity.this, ClientLoginActivity.class);
                                                startActivity(clientRegisterRun);
                                            }

                                        }
        );

        Button AdvisorButton = (Button) findViewById(R.id.Advisor);
        AdvisorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent advisorLoginRun = new Intent(MainActivity.this, AdvisorLoginActivity.class);
                startActivity(advisorLoginRun);
            }
        });

    }
}
